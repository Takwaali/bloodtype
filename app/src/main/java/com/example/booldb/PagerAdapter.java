package com.example.booldb;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class PagerAdapter extends FragmentPagerAdapter {
    private int numoftabs;
    private List<Fragment> fragments=new ArrayList<>();
    private List<String> Title =new ArrayList<>();
    public PagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm,behavior);

    }
    public  void addfragment(Fragment fragment,String title){

        fragments.add(fragment);
        Title.add(title);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {


                return  fragments.get(position);
        }




    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return Title.get(position);
    }
}
