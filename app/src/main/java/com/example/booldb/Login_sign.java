package com.example.booldb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

public class Login_sign extends AppCompatActivity {
private ViewPager viewPager;
private TabLayout tableLayout;
private Loginfragment loginfragment;
private  Signfragment signfragment;
Button submet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        viewPager = findViewById(R.id.viewpager);
        tableLayout = (TabLayout) findViewById(R.id.tabLayout);

        loginfragment = new Loginfragment();
        signfragment = new Signfragment();
        tableLayout.setupWithViewPager(viewPager);
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager(), 0);
        pagerAdapter.addfragment(loginfragment, "تسجيل دخول");
        pagerAdapter.addfragment(signfragment, "حساب جديد ");
        viewPager.setAdapter(pagerAdapter);
        submet = findViewById(R.id.submit);

    }
}