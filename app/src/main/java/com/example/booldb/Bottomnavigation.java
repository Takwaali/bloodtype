package com.example.booldb;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

public class Bottomnavigation extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottomnavigation);
        BottomNavigationView bottomNavigationView=findViewById(R.id.bt_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(navlistener);


        Toolbar toolbar= findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       drawer=findViewById(R.id.drawerLayout);
       // NavigationView navigationView=findViewById(R.id.nav_view);
      //  navigationView.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle toggle =new ActionBarDrawerToggle(this,drawer,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close);
//drawer.addDrawerListener(toggle);
//toggle.syncState();
      //if (savedInstanceState==null){
       // getSupportFragmentManager().beginTransaction().replace(R.id.frag_container,
                //new Drawer_posts()).commit();
//navigationView.setCheckedItem(R.id.nav_posts);}
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.nav_posts:
                getSupportFragmentManager().beginTransaction().replace(R.id.frag_container,
                        new Drawer_posts()).commit();
                break;
            case R.id.nav_donat:
                getSupportFragmentManager().beginTransaction().replace(R.id.frag_container,
                        new Drawer_donation()).commit();
                break;
            case R.id.nav_your_donat:
                getSupportFragmentManager().beginTransaction().replace(R.id.frag_container,
                        new Drawer_yourdonation()).commit();
                break;

            case R.id.nav_rate:
                getSupportFragmentManager().beginTransaction().replace(R.id.frag_container,
                        new Drawer_rate()).commit();
                break;
            case R.id.nav_share:
                getSupportFragmentManager().beginTransaction().replace(R.id.frag_container,
                        new Drawer_share()).commit();
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
        super.onBackPressed();}
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navlistener=
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedfragment =null;
                   switch (item.getItemId()){
                       case  R.id.nav_blood:
                           selectedfragment=new DonationFragment();
                           break;
                       case  R.id.nav_home:
                           selectedfragment=new HomeFragment();
                           break;
                       case  R.id.nav_rate:
                           selectedfragment=new RateFragment();
                           break;
                   }
                   getSupportFragmentManager().beginTransaction().replace(R.id.fagment_container,selectedfragment).commit();
                   return true;
                }
            };
}